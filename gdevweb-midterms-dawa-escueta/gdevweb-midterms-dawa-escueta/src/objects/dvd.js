class Dvd extends Phaser.Sprite {
    constructor(game, x, y, key) {
        super(game, x, y, key);

        this.anchor.setTo (0.5, 0.5);

        this.speed = 200;
        
        this.dir = {
            x: (Math.random () > 0.5) ? 1 : -1,
            y: (Math.random () > 0.5) ? 1 : -1
        };

        this.vel = {
            x: this.speed * Math.cos(Math.random()*Math.PI*2), 
            y: this.speed * Math.sin(Math.random()*Math.PI*2)
        };

        this.changeColor ();
        this.scale.setTo(0.8, 0.8);
    }

    update() {
        this.x += this.game.time.elapsed/1000 * this.vel.x;
        this.y += this.game.time.elapsed/1000 * this.vel.y;

        if (this.x < this.width * 0.5) {
            this.x = this.width * 0.5;
            this.vel.x *= -1;
            this.changeColor ();
        } else if (this.x > this.game.width - this.width * 0.5) {
            this.x = this.game.width - this.width * 0.5;
            this.vel.x *= -1;
            this.changeColor ();
        } else if (this.y < this.height * 0.5) {
            this.y = this.height * 0.5;
            this.vel.y *= -1;
            this.changeColor ();
        } else if (this.y > this.game.width - this.height * 0.5) {
            this.y = this.game.width - this.height * 0.5;
            this.vel.y *= -1;
            this.changeColor ();
        }
    }

    changeColor () {
        var planets = ["mercury", "venus", "earth", "mars", "jupiter", "saturn", "neptune", "uranus"];
        this.loadTexture(planets [Math.floor(Math.random() * planets.length)]);
    }
}
