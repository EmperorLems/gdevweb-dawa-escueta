class Menu extends Phaser.State {
    constructor() {
        super();
    }

    preload() {
        // fit to screen
        this.game.load.video('vid', 'assets/meme.mp4');
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
    }

    create() {
        this.background = this.game.add.graphics();
        this.background.beginFill (0x000000);
        this.background.drawRect (0, 0, this.game.width, this.game.height);
        this.background.endFill ();
        var shootStars = this.game.add.video('vid');
        var videoImage = shootStars.addToWorld();
        videoImage.scale.setTo (game.width /videoImage.width , game.height / videoImage.height);
        shootStars.play();
        shootStars.volume = 0;

        var style = {
            font: 'Arial',
            fontSize: 32,
            fill: "#ffffff",
            align: "center"
        };
        
        this.titleText = this.game.add.text (this.game.world.centerX, this.game.height * 0.25, "Dawa & Escueta", style);
        this.titleText.anchor.setTo (0.5, 0.5);
        style = {
            font: 'Arial',
            fontSize: 24,
            fill: "#ffffff",
            align: "center"
        };

        this.instructionText = this.game.add.text (this.game.world.centerX, this.game.height * 0.5, "Touch me to start!", style);
        this.instructionText.anchor.setTo (0.5, 0.5);

        this.input.onDown.add(function () {
            this.game.state.start('game');
        }, this);
    }
}
