var filter;

class Game extends Phaser.State {
    constructor() {
    	super();
    }

    preload() {
        this.game.load.image('spaceBG', 'assets/spaceBG.jpg');
        this.game.load.image('mercury', 'assets/mercury.png');
        this.game.load.image('mars', 'assets/mars.png');
        this.game.load.image('earth', 'assets/earth.png');
        this.game.load.image('venus', 'assets/venus.png');
        this.game.load.image('uranus', 'assets/uranus.png');
        this.game.load.image('neptune', 'assets/neptune.png');
        this.game.load.image('jupiter', 'assets/jupiter.png');
        this.game.load.image('saturn', 'assets/saturn.png');
        this.game.load.audio('music', 'assets/web.mp3')
    }

    create() {
        //this.background = this.game.add.image(0,0, 'spaceBG');
        //this.background.beginFill (0x000000);
        //this.background.drawRect (0, 0, this.game.width, this.game.height);
        //this.background.endFill ();

        var fragmentSrc = [

            "precision mediump float;",
    
            "uniform vec2      resolution;",
            "uniform float     time;",
    
            "#define PI 90",
    
            "void main( void ) {",
    
            "vec2 p = ( gl_FragCoord.xy / resolution.xy ) - 0.0;",
    
            "float sx = 0.5 + 0.5 * sin( 100.0 * p.x - 1. * pow(time, 0.5)*5.) * sin( 5.0 * p.x - 1. * pow(time, 0.9)*5.);",
    
            "float dy = 1.0/ ( 1000. * abs(p.y - sx));",
    
            "dy += 1./ (25. * length(p - vec2(p.x, 0.)));",
    
            "gl_FragColor = vec4( (p.x + 0.3) * dy, 0.3 * dy, dy, 1.1 );",
    
        "}"];
    
        filter = new Phaser.Filter(game, null, fragmentSrc);
        filter.setResolution(800, 600);
    
        var sprite = game.add.sprite();
        //sprite.scale.setTo (game.width / sprite.width , game.height / sprite.height);
        sprite.width = 800;
        sprite.height = 800;
    
        sprite.filters = [ filter ];
        
        this.dvd = this.game.world.addChild (new Dvd (this.game, this.game.world.centerX, this.game.world.centerY, 'mercury'));
        this.sound = this.game.add.audio('music');
        this.sound.play();

        
        this.input.onDown.add(function () {
            this.sound.stop();
            this.game.state.start('menu');
        }, this);
    }

    update()
    {
        filter.update();
    }
}
